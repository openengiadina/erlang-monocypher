// SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
// SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <erl_nif.h>
#include "monocypher.h"
#include "monocypher-ed25519.h"

/* Authenticated Encryption */

static ERL_NIF_TERM crypto_lock_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary mac;
    if(!enif_inspect_binary(env, argv[0], &mac)) {
        return enif_make_badarg(env);
    }

    // mac size must be exactly 16 bytes.
    if(mac.size != 16) {
        return enif_make_badarg(env);
    }

    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[1], &key)) {
        return enif_make_badarg(env);
    }

    // key size must be exactly 32 bytes.
    if(key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary nonce;
    if(!enif_inspect_binary(env, argv[2], &nonce)) {
        return enif_make_badarg(env);
    }

    // nonce size must be exactly 24 bytes.
    if(nonce.size != 24) {
        return enif_make_badarg(env);
    }

    ErlNifBinary plain_text;
    if(!enif_inspect_binary(env, argv[3], &plain_text)) {
        return enif_make_badarg(env);
    }

    // create a new binary for the cipher_text
    ERL_NIF_TERM cipher_text_term;
    unsigned char* cipher_text = enif_make_new_binary(env, plain_text.size, &cipher_text_term);

    crypto_lock(mac.data, cipher_text, key.data, nonce.data, plain_text.data, plain_text.size);

    return cipher_text_term;
}

static ERL_NIF_TERM crypto_unlock_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[0], &key)) {
        return enif_make_badarg(env);
    }

    // key size must be exactly 32 bytes.
    if(key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary nonce;
    if(!enif_inspect_binary(env, argv[1], &nonce)) {
        return enif_make_badarg(env);
    }

    // nonce size must be exactly 24 bytes.
    if(nonce.size != 24) {
        return enif_make_badarg(env);
    }

    ErlNifBinary mac;
    if(!enif_inspect_binary(env, argv[2], &mac)) {
        return enif_make_badarg(env);
    }

    // mac size must be exactly 16 bytes.
    if(mac.size != 16) {
        return enif_make_badarg(env);
    }

    ErlNifBinary cipher_text;
    if(!enif_inspect_binary(env, argv[3], &cipher_text)) {
        return enif_make_badarg(env);
    }

    // create a new binary for the plain_text
    ERL_NIF_TERM plain_text_term;
    unsigned char* plain_text = enif_make_new_binary(env, cipher_text.size, &plain_text_term);

    int unlock_return_value = crypto_unlock(plain_text, key.data, nonce.data, mac.data, cipher_text.data, cipher_text.size);

    if (unlock_return_value == 0) {
        return enif_make_tuple2(env, enif_make_atom_len(env, "ok", 2), plain_text_term);
    } else {
        return enif_make_tuple2(env, enif_make_atom_len(env, "error", 5), enif_make_int(env, unlock_return_value));
    }
}

/* Hashing */

static ERL_NIF_TERM crypto_blake2b_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ERL_NIF_TERM hash_term;
    unsigned char* hash = enif_make_new_binary(env, 64, &hash_term);

    ErlNifBinary message;
    if(!enif_inspect_binary(env, argv[0], &message)) {
        return enif_make_badarg(env);
    }

    crypto_blake2b(hash, message.data, message.size);

    return hash_term;
}

static ERL_NIF_TERM crypto_blake2b_general_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    size_t hash_size;
    if (!enif_get_ulong(env, argv[0], &hash_size)) {
        return enif_make_badarg(env);
    }

    // hash_size must be between 1 and 64.
    if(hash_size < 1 || hash_size > 64) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM hash_term;
    unsigned char* hash = enif_make_new_binary(env, hash_size, &hash_term);

    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[1], &key)) {
        return enif_make_badarg(env);
    }

    // key size must be betweeen 0 and 64
    if(key.size < 0 || key.size > 64) {
        return enif_make_badarg(env);
    }

    ErlNifBinary message;
    if(!enif_inspect_binary(env, argv[2], &message)) {
        return enif_make_badarg(env);
    }

    crypto_blake2b_general(hash, hash_size, key.data, key.size, message.data, message.size);

    return hash_term;
}

/* Password hashing and key derivation */

static ERL_NIF_TERM crypto_argon2i_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    size_t hash_size;
    if (!enif_get_ulong(env, argv[0], &hash_size)) {
        return enif_make_badarg(env);
    }

    // hash size must be exactly 32 or 64 bytes
    if(!(hash_size == 32 || hash_size == 64)) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM hash_term;
    unsigned char* hash = enif_make_new_binary(env, hash_size, &hash_term);

    size_t nb_blocks;
    if(!enif_get_ulong(env, argv[1], &nb_blocks)) {
        return enif_make_badarg(env);
    }

    // number of blocks must at least 8
    if(nb_blocks < 8) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM work_area_term;
    unsigned char* work_area = enif_make_new_binary(env, nb_blocks * 1024, &work_area_term);

    size_t nb_iterations;
    if(!enif_get_ulong(env, argv[2], &nb_iterations)) {
        return enif_make_badarg(env);
    }

    // number of iterations must at least 1
    if(nb_iterations < 1) {
        return enif_make_badarg(env);
    }

    ErlNifBinary password;
    if(!enif_inspect_binary(env, argv[3], &password)) {
        return enif_make_badarg(env);
    }

    // length of password must at least 1
    if(password.size < 1) {
        return enif_make_badarg(env);
    }

    ErlNifBinary salt;
    if(!enif_inspect_binary(env, argv[4], &salt)) {
        return enif_make_badarg(env);
    }

    // length of salt must at least 8
    if(salt.size < 8) {
        return enif_make_badarg(env);
    }

    crypto_argon2i(hash, hash_size, work_area, nb_blocks, nb_iterations, password.data, password.size, salt.data, salt.size);

    return hash_term;
}

static ERL_NIF_TERM crypto_argon2i_general_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    size_t hash_size;
    if (!enif_get_ulong(env, argv[0], &hash_size)) {
        return enif_make_badarg(env);
    }

    // hash size must be exactly 32 or 64 bytes
    if(!(hash_size == 32 || hash_size == 64)) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM hash_term;
    unsigned char* hash = enif_make_new_binary(env, hash_size, &hash_term);

    size_t nb_blocks;
    if(!enif_get_ulong(env, argv[1], &nb_blocks)) {
        return enif_make_badarg(env);
    }

    // number of blocks must at least 8
    if(nb_blocks < 8) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM work_area_term;
    unsigned char* work_area = enif_make_new_binary(env, nb_blocks * 1024, &work_area_term);

    size_t nb_iterations;
    if(!enif_get_ulong(env, argv[2], &nb_iterations)) {
        return enif_make_badarg(env);
    }

    // number of iterations must at least 1
    if(nb_iterations < 1) {
        return enif_make_badarg(env);
    }

    ErlNifBinary password;
    if(!enif_inspect_binary(env, argv[3], &password)) {
        return enif_make_badarg(env);
    }

    // length of password must at least 1
    if(password.size < 1) {
        return enif_make_badarg(env);
    }

    ErlNifBinary salt;
    if(!enif_inspect_binary(env, argv[4], &salt)) {
        return enif_make_badarg(env);
    }

    // length of salt must at least 8
    if(salt.size < 8) {
        return enif_make_badarg(env);
    }

    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[5], &key)) {
        return enif_make_badarg(env);
    }

    // length of key must at least 0
    if(key.size < 0) {
        return enif_make_badarg(env);
    }

    ErlNifBinary ad;
    if(!enif_inspect_binary(env, argv[6], &ad)) {
        return enif_make_badarg(env);
    }

    // length of additional data must at least 0
    if(ad.size < 0) {
        return enif_make_badarg(env);
    }

    crypto_argon2i_general(hash, hash_size, work_area, nb_blocks, nb_iterations, password.data, password.size, salt.data, salt.size, key.data, key.size, ad.data, ad.size);

    return hash_term;
}

/* IETF ChaCha20 */

static ERL_NIF_TERM crypto_ietf_chacha20_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary plain_text;
    if(!enif_inspect_binary(env, argv[0], &plain_text)) {
        return enif_make_badarg(env);
    }

    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[1], &key)) {
        return enif_make_badarg(env);
    }

    if(key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary nonce;
    if(!enif_inspect_binary(env, argv[2], &nonce)) {
        return enif_make_badarg(env);
    }

    if(nonce.size != 12) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM cipher_text_term;
    unsigned char* cipher_text = enif_make_new_binary(env, plain_text.size, &cipher_text_term);

    crypto_ietf_chacha20(cipher_text, plain_text.data, plain_text.size, key.data, nonce.data);

    return cipher_text_term;
}

static ERL_NIF_TERM crypto_ietf_chacha20_ctr_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary plain_text;
    if(!enif_inspect_binary(env, argv[0], &plain_text)) {
        return enif_make_badarg(env);
    }

    ErlNifBinary key;
    if(!enif_inspect_binary(env, argv[1], &key)) {
        return enif_make_badarg(env);
    }

    if(key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary nonce;
    if(!enif_inspect_binary(env, argv[2], &nonce)) {
        return enif_make_badarg(env);
    }

    if(nonce.size != 12) {
        return enif_make_badarg(env);
    }

    uint32_t ctr;
    if (!enif_get_uint(env, argv[3], &ctr)) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM cipher_text_term;
    unsigned char* cipher_text = enif_make_new_binary(env, plain_text.size, &cipher_text_term);

    crypto_ietf_chacha20_ctr(cipher_text, plain_text.data, plain_text.size, key.data, nonce.data, ctr);

    return cipher_text_term;
}


/* Public Key Signature (Ed25519) */

static ERL_NIF_TERM crypto_ed25519_public_key_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary secret_key;
    if(!enif_inspect_binary(env, argv[0], &secret_key)) {
        return enif_make_badarg(env);
    }

    if(secret_key.size != 32) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM public_key_term;
    unsigned char* public_key = enif_make_new_binary(env, 32, &public_key_term);

    crypto_ed25519_public_key(public_key, secret_key.data);

    return public_key_term;
}

static ERL_NIF_TERM crypto_ed25519_sign_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary secret_key;
    if(!enif_inspect_binary(env, argv[0], &secret_key)) {
        return enif_make_badarg(env);
    }

    if(secret_key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary public_key;
    if(!enif_inspect_binary(env, argv[1], &public_key)) {
        return enif_make_badarg(env);
    }

    if(public_key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary message;
    if(!enif_inspect_binary(env, argv[2], &message)) {
        return enif_make_badarg(env);
    }

    ERL_NIF_TERM signature_term;
    unsigned char* signature = enif_make_new_binary(env, 64, &signature_term);

    crypto_ed25519_sign(signature, secret_key.data, public_key.data, message.data, message.size);

    return signature_term;
}

static ERL_NIF_TERM crypto_ed25519_check_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ErlNifBinary signature;
    if(!enif_inspect_binary(env, argv[0], &signature)) {
        return enif_make_badarg(env);
    }

    if(signature.size != 64) {
        return enif_make_badarg(env);
    }

    ErlNifBinary public_key;
    if(!enif_inspect_binary(env, argv[1], &public_key)) {
        return enif_make_badarg(env);
    }

    if(public_key.size != 32) {
        return enif_make_badarg(env);
    }

    ErlNifBinary message;
    if(!enif_inspect_binary(env, argv[2], &message)) {
        return enif_make_badarg(env);
    }

    int check_return_value = crypto_ed25519_check(signature.data, public_key.data, message.data, message.size);

    if (check_return_value == 0) {
        return enif_make_atom_len(env, "ok", 2);
    } else {
        return enif_make_atom_len(env, "forgery", 7);
    }
}


static ErlNifFunc nif_funs[] =
{
    {"crypto_lock", 4, crypto_lock_nif},
    {"crypto_unlock", 4, crypto_unlock_nif},
    {"crypto_blake2b", 1, crypto_blake2b_nif},
    {"crypto_blake2b_general", 3, crypto_blake2b_general_nif},
    {"crypto_argon2i", 5, crypto_argon2i_nif},
    {"crypto_argon2i_general", 7, crypto_argon2i_general_nif},
    {"crypto_ietf_chacha20", 3, crypto_ietf_chacha20_nif},
    {"crypto_ietf_chacha20_ctr", 4, crypto_ietf_chacha20_ctr_nif},
    {"crypto_ed25519_public_key", 1, crypto_ed25519_public_key_nif},
    {"crypto_ed25519_sign", 3, crypto_ed25519_sign_nif},
    {"crypto_ed25519_check", 3, crypto_ed25519_check_nif}
};

ERL_NIF_INIT(monocypher, nif_funs, NULL, NULL, NULL, NULL);
