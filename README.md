<!--
SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>

SPDX-License-Identifier: CC0-1.0
-->

# erlang-monocypher

Erlang (NIF) bindings to the [Monocypher](https://monocypher.org/) crypto library.

## Monocypher

This package includes the [Monocypher](https://monocypher.org/) cryptographic library version 3.1.1, which is dedicated to the public domain via CC0-1.0.

### Functions

Only a very limited set of functions from Monocypher are bound in the Erlang module.

Contributions are very welcome! Please email pukkamustard@posteo.net.

See also the [Monocypher documentation](https://monocypher.org/manual/).

#### Authenticated Encryption

- [X] `crypto_lock`
- [X] `crypto_unlock`
- [ ] `crypto_lock_aead`
- [ ] `crypto_unlock_aead`

#### Hashing

- [X] `crypto_blake2b`
- [X] `crypto_blake2b_general`
- [ ] `crypto_blake2b_init`
- [ ] `crypto_blake2b_general_init`
- [ ] `crypto_blake2b_update`
- [ ] `crypto_blake2b_final`

#### Password hashing and key derivation

- [X] `crypto_argon2i`
- [X] `crypto_argon2i_general`

#### IETF ChaCha20

- [X] `crypto_ietf_chacha20`
- [X] `crypto_ietf_chacha20_ctr`

#### Ed25519

- [X] `crypto_ed25519_public_key`
- [X] `crypto_ed25519_sign`
- [X] `crypto_ed25519_check`

## Acknowledgments

erlang-monocypher was initially developed for the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLNet Foundation](https://nlnet.nl/) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/).

## License

LGPL-3.0-or-later
