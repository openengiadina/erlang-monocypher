% SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
% SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
%
% SPDX-License-Identifier: LGPL-3.0-or-later

-module(monocypher).
-export([crypto_lock/4,
         crypto_unlock/4]).
-export([crypto_blake2b/1,
         crypto_blake2b_general/3]).
-export([crypto_argon2i/5,
         crypto_argon2i_general/7]).
-export([crypto_ietf_chacha20/3,
         crypto_ietf_chacha20_ctr/4]).
-export([crypto_ed25519_public_key/1,
         crypto_ed25519_sign/3,
         crypto_ed25519_check/3]).
-on_load(init/0).

-define(APPNAME, monocypher).
-define(LIBNAME, monocypher).

%% Authenticated Encryption

-type crypto_lock_mac() :: <<_:128>>.
-type crypto_lock_key() :: <<_:256>>.
-type crypto_lock_nonce() :: <<_:192>>.
-spec crypto_lock(crypto_lock_mac(), crypto_lock_key(), crypto_lock_nonce(), binary()) -> binary().
crypto_lock(_,_,_,_) ->
    not_loaded(?LINE).

-spec crypto_unlock(crypto_lock_key(), crypto_lock_nonce(), crypto_lock_mac(), binary()) -> {'ok', binary()} | {'error', integer()}.
crypto_unlock(_,_,_,_) ->
    not_loaded(?LINE).

%% Hashing

-spec crypto_blake2b(binary()) -> binary().
crypto_blake2b(_) ->
    not_loaded(?LINE).

-type crypto_blake2b_hash_size() :: 1..64.
-type crypto_blake2b_key() :: <<_:_*8>>.
-spec crypto_blake2b_general(crypto_blake2b_hash_size(), crypto_blake2b_key(), binary()) -> binary().
crypto_blake2b_general(_,_,_) ->
    not_loaded(?LINE).

%% Password hashing and key derivation

-type crypto_argon2i_hash_size() :: 32..64.
-type crypto_argon2i_nb_blocks() :: pos_integer().
-type crypto_argon2i_nb_iterations() :: pos_integer().
-type crypto_argon2i_password() :: <<_:_*8>>.
-type crypto_argon2i_salt() :: <<_:_*8>>.
-spec crypto_argon2i(crypto_argon2i_hash_size(), crypto_argon2i_nb_blocks(), crypto_argon2i_nb_iterations(), crypto_argon2i_password(), crypto_argon2i_salt()) -> binary().
crypto_argon2i(_,_,_,_,_) ->
    not_loaded(?LINE).

-type crypto_argon2i_key() :: <<_:_*8>>.
-type crypto_argon2i_ad() :: <<_:_*8>>.
-spec crypto_argon2i_general(crypto_argon2i_hash_size(), crypto_argon2i_nb_blocks(), crypto_argon2i_nb_iterations(), crypto_argon2i_password(), crypto_argon2i_salt(), crypto_argon2i_key(), crypto_argon2i_ad()) -> binary().
crypto_argon2i_general(_,_,_,_,_,_,_) ->
    not_loaded(?LINE).

%% IETF ChaCha20

-type crypto_ietf_chacha20_key() :: <<_:256>>.
-type crypto_ietf_chacha20_nonce() :: <<_:96>>.
-spec crypto_ietf_chacha20(binary(), crypto_ietf_chacha20_key(), crypto_ietf_chacha20_nonce()) -> binary().
crypto_ietf_chacha20(_,_,_) ->
    not_loaded(?LINE).

-spec crypto_ietf_chacha20_ctr(binary(), crypto_ietf_chacha20_key(), crypto_ietf_chacha20_nonce(), pos_integer()) -> binary().
crypto_ietf_chacha20_ctr(_,_,_,_) ->
    not_loaded(?LINE).

%% Public Key Signature (Ed25519)

-type crypto_ed25519_secret_key() :: <<_:256>>.
-type crypto_ed25519_public_key() :: <<_:256>>.
-type crypto_ed25519_signature() :: <<_:512>>.
-type crypto_ed25519_message() :: <<_:_*8>>.

-spec crypto_ed25519_public_key(crypto_ed25519_secret_key()) -> crypto_ed25519_public_key().
crypto_ed25519_public_key(_) ->
    not_loaded(?LINE).

-spec crypto_ed25519_sign(crypto_ed25519_secret_key(), crypto_ed25519_public_key(), crypto_ed25519_message()) -> crypto_ed25519_signature().
crypto_ed25519_sign(_,_,_) ->
    not_loaded(?LINE).

-spec crypto_ed25519_check(crypto_ed25519_signature(), crypto_ed25519_public_key(), crypto_ed25519_message()) -> 'ok' | 'forgery'.
crypto_ed25519_check(_,_,_) ->
    not_loaded(?LINE).


init() ->
    SoName = case code:priv_dir(?APPNAME) of
        {error, bad_name} ->
            case filelib:is_dir(filename:join(["..", priv])) of
                true ->
                    filename:join(["..", priv, ?LIBNAME]);
                _ ->
                    filename:join([priv, ?LIBNAME])
            end;
        Dir ->
            filename:join(Dir, ?LIBNAME)
    end,
    erlang:load_nif(SoName, 0).

not_loaded(Line) ->
    erlang:nif_error({not_loaded, [{module, ?MODULE}, {line, Line}]}).
